import gym
import numpy as n
import matplotlib.pyplot
import math

env = gym.make('CartPole-v1')
start = [0.0, 0.0, 0.0, 0.0]
Z = 1.0
seg2 = 0.001
gamma = 1
alpha = 0.5
M = 50
MAX_STEPS = 1000
iterations = 200
falling_angle = 0.21
out_track = 2.4

gprev = n.zeros(4)
alphai = n.ones(4)*0.5

def runIterations(type,avg):
    Js = n.zeros(iterations+1)
    w = n.zeros(4)
    print "Starting app with step type "+ type+" and averaging"+ str(avg)
    for i in range(iterations):
        print "iteration " + str(i+1)
        Js[i] = evaluate_policy(w,avg)
        w = do_gradient_ascent(w,Js[i],avg,type)
    Js[iterations] = evaluate_policy(w,avg)

    # plot the data
    x = n.arange(iterations+1)
    matplotlib.pyplot.plot(x, Js, label="Evaluation for "+ type + " with averaging "+str(avg)+" times")
    matplotlib.pyplot.ylabel('Evaluation')
    matplotlib.pyplot.xlabel('Iteration')
    matplotlib.pyplot.legend()
    matplotlib.pyplot.savefig(type+"_"+str(avg)+"_avg.png")
    matplotlib.pyplot.show()


def do_gradient_ascent(w,J,avg,type):
    dw = n.zeros((M,4))
    dJ = n.zeros(M)
    # collect data
    for i in range(M):
        dw[i] = n.random.uniform(-1, 1, size=4)
        new_w = w + dw[i]
        newJ = evaluate_policy(new_w,avg)
        dJ[i] = newJ - J
    # calcuate new W
    return updatew(w,dw, dJ, type)

#evaluate policy by averaging
def evaluate_policy(ws,avg):
    Js=n.zeros(avg)
    for i in range(avg):
        Js[i] = do_episode(ws)
    return n.mean(Js)

def do_episode(ws):
    env.reset()
    J = 0
    step = 1
    s = start
    done = False
    while step < MAX_STEPS and abs(s[2]) < falling_angle and abs(s[0]) < out_track :
        env.render(close = True)
        a = geta(s, ws)

        if a > 0:
            s, r, done, info = env.step(1)
        else:
            s, r, done, info = env.step(0)
        if(r == 0.0):
            r = -1.0
        # accumlate reward for episode
        J = gamma * J + r
        step += 1

    return J


def updatew(w,dW, dJ, type="normalized"):
    global alpha
    dwt = dW.transpose()
    a1 = n.dot(dwt, dW)
    a2 = n.dot(n.linalg.inv(a1), dwt)
    gfd = n.dot(a2, dJ)
    if (type == "heuristic"):
        alpha = alpha / 10.0

    if type != "adaptive":
        return (w + alpha * (gfd / (n.linalg.norm(gfd))))
    else:
        return adapt_alpha(gfd,w)



def adapt_alpha(gfd,w):

    for i in range(len(w)):
        if (gfd[i] * gprev[i] > 0):
            alphai[i] *= 1.2
            w[i] = w[i] + (alphai[i] * n.sign(gfd[i]))
            gprev[i] = gfd[i]
        elif (gfd[i] * gprev[i] < 0):
            alphai[i] *= 0.5
            w[i] = w[i] + (alphai[i] * n.sign(gfd[i]))
            gprev[i] = 0
        else:
            w[i] = w[i] +  (alphai[i] * n.sign(gfd[i]))
            gprev[i] = gfd[i]
    return w

def geta(s, ws):
    return n.random.normal(n.dot(ws.transpose(), s),(seg2**0.5), None)

#normalized
#heuristic
#adaptive
runIterations(type = "heuristic",avg = M)
