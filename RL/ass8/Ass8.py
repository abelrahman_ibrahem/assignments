import gym
import numpy as n
import matplotlib.pyplot
import math

env = gym.make('CartPole-v1')
start = [0.0, 0.0, 0.0, 0.0]
Z = 1.0
seg2 = 0.5**2
gamma = 1
alpha = 0.5
M = 50
MAX_STEPS = 1000
iterations = 200
falling_angle = 0.21
out_track = 2.4

gprev = n.zeros(4)
alphai = n.ones(4)*0.5

def runIterations(type,avg):
    Js = n.zeros([iterations+1])
    w = n.zeros(4)
    print "Starting app with step type "+ type+" and averaging"+ str(avg)
    for i in range(iterations):
        print "iteration " + str(i+1)

        w = do_gradient_ascent(w,avg, type)

        estimate = evaluate_policy(w,avg)
        Js[i] = estimate

    Js[iterations] = n.mean(evaluate_policy(w,avg))

    # plot the data
    x = n.arange(iterations+1)
    matplotlib.pyplot.plot(x, Js, label="Evaluation for "+ type + " step and "+Method +"with averaging "+str(avg)+" times")
    matplotlib.pyplot.ylabel('Evaluation')
    matplotlib.pyplot.xlabel('Iteration')
    matplotlib.pyplot.legend()
    matplotlib.pyplot.savefig(type+"_"+Method +"_"+str(avg)+"_avg.png")
    matplotlib.pyplot.show()


def do_gradient_ascent(w,avg,type):
    gradients = n.zeros(4)
    gradient= n.zeros(4)
    gradientR = n.zeros(4)
    b1 = n.zeros(4)
    b2 = n.zeros(4)
    trajectories = []
    if(Method !="PO" ):
        R=0
        for i in range(M):
            trajectories.append( do_episode(w, Method))
            R+= trajectories[i][0]

        for i in range(len(trajectories)):
            [r,grad] = trajectories[i]
            gradient = n.zeros(4)
            gradient += grad
            gradientR += grad*r
            b1 += n.power(grad, 2) * r
            b2 += n.power(grad, 2)

        b1/=M
        b2/=M
        gradientR /= M
        gradient /= M
        b = n.divide(b1,b2)
        if(Method == "reinforce"):
            gradients = gradientR-gradient*b
        else:
            gradients = gradientR

        gradients /= n.linalg.norm(gradients)
        #gradients += .1
        return updatew(w, 0, gradients, Method, type)
    else:
        b1 = []
        b2 = []
        for j in range(M):
            trajectories.append( do_episode(w, Method))
        for j in range(len(trajectories)):
            for t in len(j):

                gradients=[]
                gradientsR = []
                [R,grad] = trajectories[j]
                if(len(b1)<t):
                    b1.append(n.power(grad, 2) * R)
                    b2.append(n.power(grad, 2))
                    gradients.append(grad)
                    gradientsR.append(grad*R)
                else:
                    b1[t] += n.power(grad, 2) * R
                    b2[t] += n.power(grad, 2)
                    gradients[t] +=grad
                    gradientsR[t] +=grad * R

        b_array = [n.divide(b1[i], b2[i]) for i in range(len(b1))]
        for a,b,c in zip(b_array,gradients,gradientsR):
            gradient += c-(n.multiply(a,b))/M
        gradients /= n.linalg.norm(gradients)
        return updatew(w, 0, gradient, Method, type)





#evaluate policy by averaging
def evaluate_policy(ws,avg):
    Js = n.zeros(avg)
    for i in range(avg):
        [Js[i],a] = do_episode(ws)
    return n.mean(Js)

def do_episode(ws,method ="vanilla"):
    env.reset()
    J = n.zeros(4)
    R = 0
    step = 1
    s = start
    done = False
    steps=[]
    while step < MAX_STEPS and abs(s[2]) < falling_angle and abs(s[0]) < out_track :
        env.render(close = True)
        a = geta(s, ws)

        #if a > 0:
         #   s, r,done,info = cartPole(s,a)
        #else:
        s, r = cartPole(s,a)
        if(r == 0.0):
            r = -1.0
        # accumlate reward for episode

        R = gamma *R +r
        #print ws,s,a
        x=(-1/seg2)
        x*= s
        x*=(n.dot(ws.transpose(),s)-a)
        J =  J + x
        steps.append([R,J])
        step += 1

    return [R,J]


def updatew(w,dW, dJ=0,method="vanilla", type="normalized"):
    global alpha
    if method =="vanilla" or method=="reinforce" or method=="PO":
        if (type == "heuristic"):
            alpha = alpha / 10.0

        if type != "adaptive":
            return (w + alpha * (dJ))
        else:
            return adapt_alpha(dJ, w)
    else:

        dwt = dW.transpose()
        a1 = n.dot(dwt, dW)
        a2 = n.dot(n.linalg.inv(a1), dwt)
        gfd = n.dot(a2, dJ)
        if (type == "heuristic"):
            alpha = alpha / 10.0

        if type != "adaptive":
            return (w + alpha * (gfd / (n.linalg.norm(gfd))))
        else:
            return adapt_alpha(gfd,w)



def adapt_alpha(gfd,w):

    for i in range(len(w)):
        if (gfd[i] * gprev[i] > 0):
            alphai[i] *= 1.2
            w[i] = w[i] + (alphai[i] * n.sign(gfd[i]))
            gprev[i] = gfd[i]
        elif (gfd[i] * gprev[i] < 0):
            alphai[i] *= 0.5
            w[i] = w[i] + (alphai[i] * n.sign(gfd[i]))
            gprev[i] = 0
        else:
            w[i] = w[i] +  (alphai[i] * n.sign(gfd[i]))
            gprev[i] = gfd[i]
    return w

def geta(s, ws):
    return n.random.normal(n.dot(ws.transpose(), s),(seg2**0.5), None)

def calc_policy(ws,s,a):
    return (1/Z)*n.exp((-1*(n.dot(ws.transpose(), s) - a)**2)/(2*seg2))

def cartPole(state,action):
    GRAVITY = 9.8
    MASSCART = 1.0
    m1 = .1
    m2 = MASSCART + m1
    LENGTH = .5
    POLE = m1*LENGTH
    FORCE_MAG = 10.
    TAU = .02

    theta = state[2]
    theta_dot = state[3]
    x = state[0]
    x_dot = state[1]

    eps1 = n.random.normal(0.,.01,None)
    eps2 = n.random.normal(0.,  0.0001,None)

    temp = (action+ POLE*theta_dot**2*math.sin(theta))/m2
    theta_acc = (GRAVITY*math.sin(theta)-math.cos(theta)*temp)/(LENGTH*(4./3.-m1*math.cos(theta)**2/m2))
    x_acc = temp - POLE*theta_acc*math.cos(theta)/m2
    next_x = x + TAU*x_dot + eps1
    next_x_dot = x_dot + TAU*x_acc + eps1
    next_theta = theta + TAU*theta_dot + eps2
    next_theta_dot = theta_dot + TAU*theta_acc + eps2

    if abs(next_theta)>falling_angle or abs(next_x)>out_track:
        reward = -1
    else:
        reward = 1

    return n.array([next_x,next_x_dot,next_theta,next_theta_dot]),reward
#normalized
#heuristic
#adaptive
Method="reinforce"#vanilla,#reinforce,#PO
runIterations(type = "PO",avg = 1)

