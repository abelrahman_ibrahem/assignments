import numpy
import matplotlib.pyplot as plt
s_state= 3
A=(1,-1)
no_episodes = 100

def montecarlo(alpha, values,episode):
	for j in range(5):
		count =0.0
		for ep in episode:
			if ep == j:
				count+=1
		if count>0:
			values[j]+=alpha *(1/count -values[j])
	
	return values
def temporalDifference(alpha, values,c_state,action):
	nextstate =c_state+action
	vs = values[c_state-1]
	vsprime = values[nextstate-1]
	r=0
	if nextstate ==6:
		r=1
	values[c_state-1]+= alpha*(r+vsprime-vs)	
	return values
	
#montecarlo
def doEpisodes(eps,alph,c_state,prnt,mc):
	values=[0,0,0,0,0,1]
	
	for i in range(eps):		
		current_state=c_state
		if prnt:
			print "episode "+str(i)+": ",
		episode=[current_state]
		while(current_state != 6 and current_state!=0):
	#		if(values[current_state]>values[current_state-2]):
	#			current_state+= 1
	#		elif(values[current_state-2]>values[current_state]):
	#			current_state -= 1
	#		else:
			random_action=A[numpy.random.choice(numpy.arange(0, 2), p=[0.5,0.5])]
			if not mc:
				values =temporalDifference(alph,values,current_state,random_action)		
			current_state+=random_action			
			episode.append(current_state-1)

		if prnt:
 			print episode
		if(current_state == 6 and mc):
			values = montecarlo(alph,values,episode)
			if prnt:
				print values


	return values
def getRMS(values):
	perfect=[1.0/6.0,2.0/6.0,3.0/6.0,4.0/6.0,5.0/6.0]
	rms =0
	for i in range(5):
		rms += (perfect[i]-values[i])**2
	return (rms/5.0)
def calcForAlpha(alpha,mc):
	rmses=[]

	for i in range(no_episodes+1):		
		v = doEpisodes(i,alpha,s_state,False,mc)
		method = "MC"
		if not mc:
			method ="TD"
		print method+" with alpha("+str(alpha)+") : "+str(i),
		print "v is "+str(v)
		rms = getRMS(v)
		rmses.append(rms)
		print "rms is "+str(rms)
	return rmses
#print "starting"
rms1 = calcForAlpha(0.01,True)
#print rms1
rms2 = calcForAlpha(0.02 ,True)
rms3 = calcForAlpha(0.03,True)
rms4 = calcForAlpha(0.04,True)

#print rms1
rmstd2 = calcForAlpha(0.1 ,False)
rmstd3 = calcForAlpha(0.05,False)
rmstd4 = calcForAlpha(0.15,False)
x = numpy.arange(101)
plt.plot(x, rms1,label = "MC alpha=0.01")
plt.plot(x, rms2,label = "MC alpha=0.02")
plt.plot(x, rms3,label = "MC alpha=0.03")
plt.plot(x, rms4,label = "MC alpha=0.04")

plt.plot(x, rmstd2,label = "TD alpha=0.1")
plt.plot(x, rmstd3,label = "TD alpha=0.05")
plt.plot(x, rmstd4,label = "TD alpha=0.15")

plt.axis([0, 100, 0, 0.3])
plt.legend()
plt.show()

