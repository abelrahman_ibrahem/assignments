import gym
import numpy
#import matplotlib.pyplot as plt
import math

gamma= 0.99
alpha = 0.1
epsilon = 0.1
lamda = 0.5

X_UPPER = 0.5
X_LOWER =-1.2
X_DISCRETE= 20.0

Y_UPPER = 0.07
Y_LOWER = -0.07
Y_DISCRETE= 20.0

N_OF_ACTIONS= 3
Q = numpy.zeros(shape=(int(X_DISCRETE),int(Y_DISCRETE),N_OF_ACTIONS))
E = numpy.zeros(shape=(int(X_DISCRETE),int(Y_DISCRETE),N_OF_ACTIONS))
MAX_STEP_EPISODE=10**5
EPISODES=100
timesteps=[0]*EPISODES
start = [0,0]
env = gym.make('MountainCar-v0')
REPEAT=10
positions=[]
velocities=[]
def repeat_learning():
	Average = [0]*EPISODES
	Q = numpy.zeros(shape=(int(X_DISCRETE),int(Y_DISCRETE),N_OF_ACTIONS))
	print "Starting iteration"
	env = gym.make('MountainCar-v0')
	print "Starting iteration"
	for x in range(REPEAT):
		
		ALL_timesteps = do_learning(x)
		print "finished trial "+ str(x)
		for j in range(EPISODES):
			Average[j]+= ALL_timesteps[j]
	#Averaging and showing
	for j in range(EPISODES):
		Average[j]/=REPEAT
	#show Averaging
	
	x = numpy.arange(EPISODES)
	plt.plot(x,Average,label = "Average")
	plt.ylabel('Time Steps for '+str(X_DISCRETE)+" dicretization")
	plt.xlabel('Episodes')
	plt.legend()
	plt.show()
	

def do_learning(n):
	timesteps=[0]*EPISODES	
	
	print "start learning"
	Q = numpy.zeros(shape=(int(X_DISCRETE),int(Y_DISCRETE),N_OF_ACTIONS))
	for i in range(EPISODES):
		timesteps[i] = do_episode()
		if(timesteps[i]>=MAX_STEP_EPISODE):
			print "Exceed no of timesteps :: ",
		print "finished Episode "+ str(n)+"."+str(i)+" in "+str(timesteps[i])+" steps"
	#show result of learning
	x = numpy.arange(EPISODES)
	plt.plot(x,timesteps,label = "Trial: "+str(n+1))
	plt.ylabel('Time Steps')
	plt.xlabel('Episodes')	
	return timesteps

def do_episode():
	steps = 0
	s = start
	E = numpy.zeros(shape=(int(X_DISCRETE),int(Y_DISCRETE),N_OF_ACTIONS))
	global env	
	env.reset()
	a = epsilonGreedy(s)
	print Q
	while s[0]<0.5 and steps<MAX_STEP_EPISODE:
		#learn
		x = getDisceretePosition(s[0])
		y = getDiscereteVelocity(s[1])
		if (not y in velocities ):
			print "first time to see velocity",y
			velocities.append(y)

		if ( not x in positions):
			print "first time to see position",x
			positions.append(x)

		env.render()
		sprime, r, done, info = env.step(a)
		#print "step "+str(steps)+ ", a = "+str(a)+", current position ",s," r=",r,"info =",info
		#[r,sprime] = update_state(s,a)
		adash = epsilonGreedy(sprime)
		astar = findmax(sprime)
		delta = r+ getq(sprime,astar)-getq(s,a)
		E[x,y,a] = 1
		#print E[x,y,a]
		for i in range(len(Q)):
			for j in range(len(Q[0])):
				for k in range(len(Q[0,0])):					
					Q[i,j,k]+= alpha*delta*E[i,j,k]
					if adash == astar:
						E[i,j,k] = gamma * lamda* E[i,j,k]
					else:
						E[i,j,k] = 0
					
		#print Q
		s = sprime
		
		a = astar
		#print steps
		steps+=1
	
	return steps
def getq(s,a):
	return Q[getDisceretePosition(s[0]),getDiscereteVelocity(s[1]),a]
def random(current_pos):	
	random_action = numpy.random.choice(numpy.arange(-1, 2), p =  getprobability(current_pos))
	return random_action + 1 

def epsilonGreedy(current_pos):
	max_action =  findmax(current_pos)	
	random_action = numpy.random.choice(numpy.arange(-1, 2), p =  getprobability(current_pos))
	action = numpy.random.choice([max_action,random_action+1],p=[1-epsilon,epsilon])	
	#if action == max_action:
	#	print "max action"
	#elif action == random_action :
	#	print "random action"

	return action
def findmax(current_pos):
	max_action=-1
	max_q=-100000000
	x = getDisceretePosition(current_pos [0])
	y = getDiscereteVelocity(current_pos[1])
	#print current_pos,x,y
	normalvalue = -100000000
	for i in range(N_OF_ACTIONS):
		
		if Q[x,y,i] > max_q:
			max_action = i
			max_q = Q[x,y,i]
		else:
			normalvalue= Q[x,y,i]
	if max_q == normalvalue:
		#print "all actions are equal picking random one",getprobability(current_pos)		
		max_action = numpy.random.choice(numpy.arange(N_OF_ACTIONS), p =  getprobability(current_pos))			
	#print "Q(",x,y,max_action,")= ",Q[x,y,max_action], " is the maximum value among ",
	#print map(lambda i: Q[x,y,i], range(N_OF_ACTIONS))
	return max_action 
	
def getprobability(current_pos):
		p=[1]*N_OF_ACTIONS
		for i in range(N_OF_ACTIONS):
			p[i]= 1.0/(N_OF_ACTIONS)
		return p

def update_state(current_state,action):
	x = current_state[0]
	y=  current_state[1]
	a = action -1
	x = x + y
	y = y + (0.001*a)-(0.0025 *math.cos(3*current_state[0]))

	if (x >= 0.5):
		return [0,[x,y]]
	elif (x<=-1.2):
		return [-1,[0,y]]	
	else:
		return [-1,[x,y]]


def getDisceretePosition(pos):
	disp= 0-X_LOWER;	
	return int(math.floor(((pos+disp)*(X_DISCRETE-1))/(X_UPPER+disp)))

def getDiscereteVelocity(pos):
	disp= 0-Y_LOWER;
	return int(math.floor(((pos+disp)*(Y_DISCRETE-1))/(Y_UPPER+disp)))
print "start"
repeat_learning()
