from scipy import *
import numpy as np
import numpy.random as rand
import copy
Pa1 = np.zeros((25,25))

S = np.zeros((25,2))
row,column =0,0

for  i in range(25):
    S[i] =(row,column)
    column+=1
    if column >4:
        row +=1
        column=0

Pa1[0,1] = 1
Pa1[1,2] = 1
Pa1[2,3] = 1
Pa1[3,4] = 1
Pa1[5,0] = 1
Pa1[6,7] = 1
Pa1[7,8] = 1
Pa1[8,3] = 1
Pa1[9,4] = 1
Pa1[10,5] = 1
Pa1[11,6] = 1
Pa1[12,7] = 1
Pa1[13,8] = 1
Pa1[14,9] = 1
Pa1[15,10] = 1
Pa1[16,11] = 1
Pa1[17,18] = 1
Pa1[18,13] = 1
Pa1[19,14] = 1
Pa1[20,15] = 1
Pa1[21,22] = 1
Pa1[22,23] = 1
Pa1[23,24] = 1
Pa1[24,19] = 1




R = np.array(25)
gamma = 0.99
epsilon = 0.3

def optimal_policy(s):
    s_i= getIofS(s)
    for i in range(25):
        if Pa1[s_i,i] ==1 and i == 4:
            return [1,S[i]]
        elif Pa1[s_i,i] ==1:
            return [0, S[i]]

def getIofS(s):
    return int((s[0]*5) +s[1])
def get_max_a(s,Q):
    [r, sprime] = do_action(s, 0)
    v = (Q[getIofS(sprime), 0] * gamma + r)
    maxValue = v
    max_a = 0
    all_equal = True
    for i in range(1, 4):
        [r, sprime] = do_action(s, i)
        v = (Q[getIofS(sprime), i] * gamma + r)
        if maxValue < v:
            maxValue = v
            max_a = i
            all_equal = False
        elif maxValue > v:
            all_equal = False
    if all_equal:
       max_a = rand.randint(0,3)
    return max_a

def greedy_policy(s,Q):
    [r, sprime] = do_action(s,0)
    v = (Q[getIofS(sprime),0] * gamma + r)
    maxValue = v
    max_a = 0
    all_equal = True
    for i in range(1,4):
       [r,sprime] = do_action(s,i)
       v =  (Q[getIofS(sprime),i] * gamma + r)
       if maxValue < v:
           maxValue = v
           max_a = i
           all_equal=False
       elif maxValue > v:
           all_equal=False
    if all_equal:
       return rand.randint(0,4)
    return np.random.choice([max_a,rand.randint(0,4)],p=[1-epsilon,epsilon])

def do_action(s,a):
    sprime = copy.copy(s)
    if a ==0:
        sprime[0]-=1
    elif a == 1:
        sprime[0] += 1
    elif a == 2:
        sprime[1] -= 1
    elif a == 3:
        sprime[1] += 1

    if(sprime[0] < 0):
        sprime[0]=0

    if (sprime[1] < 0):
        sprime[1] = 0

    if (sprime[1] > 4):
        sprime[1] = 4

    if (sprime[0] > 4):
        sprime[0] = 4

    if(getIofS(sprime) == 4):
        return [1,sprime]
    else:
        return [0,sprime]

def get_P(Q):
    Pa = np.zeros((25, 25))
    for i in range(25):
        a = get_max_a(S[i],Q)
        [r,sprime] = do_action(S[i],a)
        j = getIofS(sprime)
        Pa[i,j]=1
    return Pa
def trajectory(Q,start=20):
    r=0
    s= S[start]
    print "start is ",S[start],start
    while r!=1:
        a = greedy_policy(s,Q)
        [r,sprime] = do_action(s,a)
        print r
        Q[getIofS(s),a] = Q[getIofS(s),a] +(r + gamma* Q[getIofS(sprime),get_max_a(sprime,Q)]- Q[getIofS(s),a])
        s= sprime

def evaluate_policy(start=20):
    Q = np.zeros((25, 4))
    for i in range(200):
        print S
        print "=============================================================="
        trajectory(Q,start=20)
    getR(get_P(Q))

def getR(Pa):
    prod = np.inner((Pa1-Pa),(np.eye(25)-gamma * Pa1))

    for i in range(25):
        pai = Pa[i]
        pa1i = Pa1[i]

    print prod
evaluate_policy()