import Learner
import numpy
import matplotlib.pyplot as plt
from thread import start_new_thread
import threading as thrd
import ClifLearner

wind=[0,0,0,1,1,1,2,2,1,0]
r= -1
start = [6,0]
G=[3,7]
Q1 = numpy.zeros(shape=(7,10,4))
Q2 = numpy.zeros(shape=(7,10,4))
Q_cliff = numpy.zeros(shape=(4,12,4))
start_cliff = [3,0]
G_cliff=[3,11]
alpha=0.1
epsilon =0.1
n=100
Gs=[[4,7],[2,7], [2,8],[3,8],[4,8],[4,6],[3,6],[2,6]]
learner1_sarsa = Learner.learner(start, G , Q1, alpha, epsilon, n, 'sarsa',wind, r)
learner1_q = Learner.learner(start, G, Q2, alpha, epsilon, n, 'q-learning',wind, r)
learner_clif_sarsa = ClifLearner.ClifLearner(start_cliff, G_cliff, Q_cliff, alpha, epsilon, n, 'sarsa-clif', r)
learner_clif_q = ClifLearner.ClifLearner(start_cliff, G_cliff, Q_cliff, alpha, epsilon, n, 'q-learning-clif', r)
qlearners=[None]*8
#print "starting q"
#start_new_thread(learner1_q.start,())
#print "starting sarsa"
#start_new_thread(learner1_sarsa.start,())
print "starting sarsa for clif"
start_new_thread(learner_clif_sarsa.start,())
print "starting q for clif"
start_new_thread(learner_clif_q.start,())

#for i  in range(8):
#	Qi = numpy.zeros(shape=(7,10,4))
#	learneri = Learner.learner(start, Gs[i], Qi, alpha, epsilon, n, 'q-learning'+str(Gs[i]),wind, r)
#	print "starting learner"+str(i)
#	start_new_thread(learneri.start,())	


input("Press enter to exit\n")
exit
