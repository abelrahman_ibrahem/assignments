import numpy
import matplotlib.pyplot as plt
import threading as thrd
class learner:
	counter =0
	lck = thrd.Lock()
	def __init__(self,start = [3,0], G=[3,7], Q = numpy.zeros(shape=(7,10,4)), alpha=0.1, epsilon=0.1, n=100, typ='sarsa',wind=[0,0,0,1,1,1,2,2,1,0] ,r=-1):
		self.G = G
		self.S = start
		self.Q = Q
		self.eps = epsilon
		self.alpha = alpha
		self.n = n
		self.typ = typ
		self.wind = wind
		self.r = r
	@staticmethod
	def getcounter():
		return learner.counter
	@staticmethod
	def inc():
		learner.lck.acquire()
		learner.counter+=1
		learner.lck.release()
	def start(self):
		#print '======================'+self.typ+'==================='
		n_timesteps = self.timeSteps(self.n,self.typ)

		print "finished "+ self.typ+" last in  "+str(n_timesteps[self.n-1])
		x = numpy.arange(self.n)
		learner.lck.acquire()
		plt.plot(x,n_timesteps,label = self.typ)
		learner.lck.release()
		#plt.axis([-1,self.n , -1, 10000])
		self.visualise_policy()
		#if(learner.getcounter() ==0):
		#	plt.ylabel('Time Steps')
		#	plt.xlabel('Episodes')
		#	plt.legend()
		#	plt.show()
		#	learner.inc()
		

	def timeSteps(self,n,typ):
	
		n_timesteps = numpy.zeros(shape=(n))
		for i in range(n):
			current_pos = self.S
			new_pos = self.S
			#if i != 0:
			#	n_timesteps[i]=n_timesteps[i-1]
		#	print self.typ+" :: Episode: "+str(i)
			while new_pos != self.G :
			#	print "Episode"+str(i)+": time step="+str(n_timesteps[i])+", current_pos= ",str(current_pos)
				a =  self.epsilonGreedy(current_pos)
				new_pos =  self.update_state(a,current_pos)
		#		print "a = " + str(a) + ", Q(s,a)=" + str(Q[current_pos[0],current_pos[1],a])
				if typ == "sarsa":
					aprime =  self.epsilonGreedy(new_pos)
				else:
					aprime =  self.findmax(new_pos)
				Qsaprime =  self.findQ(new_pos,aprime)
		#		print "a' = " + str(aprime) + ", s'="+str(new_pos)+", Q(s',a')=" + str(Qsaprime)
		#		print current_pos
				self.Q[current_pos[0],current_pos[1],a] += self.alpha*(self.r+Qsaprime-self.Q[current_pos[0],current_pos[1],a])
				current_pos = new_pos
		#		print Q
				n_timesteps[i]+=1
		return n_timesteps


	def epsilonGreedy(self,current_pos):
		max_action =  self.findmax(current_pos)	
		random_action = numpy.random.choice(numpy.arange(0, 4), p =  self.getprobability(current_pos))
		action = numpy.random.choice([max_action,random_action],p=[1-self.eps,self.eps])	
		#if action == max_action:
			#print "max action"
		#elif action == random_action :
			#print "random action"
		return action

	def getprobability(self,current_pos):
		p=[0.25,0.25,0.25,0.25]
		n_zeros=0

		if current_pos[0]== 0:
			p[0] = 0
			n_zeros +=1

		if current_pos[0]== 6:
			p[1] = 0
			n_zeros +=1

		if current_pos[1]== 9:
			p[2] = 0
			n_zeros +=1

		if current_pos[1]== 0:
			p[3] = 0
			n_zeros +=1

		if n_zeros>0:
			for i in range(4):
				if p[i] !=0:
					p[i]= 1.0/(4-n_zeros)
		return p
						
	def findmax(self,current_pos):
		max_action=-1
		max_q=-100000000
		x= current_pos [0]
		y = current_pos[1]
		normalvalue = -100000000
		for i in range(4):
			if ( x==0 and i ==0) or( x==6 and i ==1) or ( y==9 and i ==2) or (y == 0 and i ==3):
				continue 
			if self.Q[x,y,i] > max_q:
				max_action = i
				max_q = self.Q[x,y,i]
			else :
				normalvalue= self.Q[x,y,i]

		if max_q == normalvalue:		
			max_action = numpy.random.choice(numpy.arange(0, 4), p =  self.getprobability(current_pos))			
	#	print "maximum action is "+str(max_action)+" with Q = "+str( Q[x,y,max_action])
		return max_action	

	def findQ(self,current_pos,action):
		return self.Q[current_pos[0],current_pos[1],action]

	def update_state(self,action,current_pos):
		x = current_pos[0]
		y = current_pos[1]
		if action == 0 and x!=0:
			x-=1
		if action == 1 and x!=6:
			x+=1
		if action == 2 and y!=9:
			y+=1
		if action == 3 and y!=0:
			y-=1
		#print "wind is "+ str(wind[y])
		x-= self.wind[y]

		if(x <0):
			x =0
		return [x,y]

	def visualise_policy(self):
		print ""
		for i in range(len(self.wind)):
			print '| '+ str(self.wind[i])+' ',
		print ""
		for i in range(len(self.Q)):
			for j in range (len(self.Q[0])):
				if [i,j] == self.G:
					print '| G ',
					continue
				maxaction = -1
				mavalue=-10000000
				for k in range(len(self.Q[0,0])):
					if mavalue < self.Q[i,j,k] and self.Q[i,j,k] !=0:
						maxaction = k
						mavalue = self.Q[i,j,k]
				if maxaction == 0:
					print '| '+u'\u25b2'+' ',
				elif maxaction == 1:
					print '| '+u'\u25bc'+' ',
				elif maxaction == 2:
					print '| '+u'\u25b6'+' ',
				else:
					print '| '+u'\u25c0'+' ',
			print'|'
		print self.Q


	def print_policy(self):
		print ""
		#for i in range(10):
		#	print '| '+ str(self.wind[i])+' ',
		#print "|"
		for i in range(len(self.Q)):
			for j in range (len(self.Q[0])):
				if [i,j] == self.G:
					print '| G ',
					continue
				maxaction = -1
				mavalue=-10000000
				for k in range(len(self.Q[0,0])):
					if mavalue < self.Q[i,j,k] and self.Q[i,j,k] !=0:
						maxaction = k
						mavalue = self.Q[i,j,k]
				print '| '+str(mavalue)+' ',
			print'|'


