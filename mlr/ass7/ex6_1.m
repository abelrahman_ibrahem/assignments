
file = dir('/home/abdo/mlr/ass7/yalefaces_cropBackground');
NF = length(file);
X=[];
%question 2.a
for k = 3 : NF
    f = fullfile('/home/abdo/mlr/ass7/yalefaces_cropBackground',file(k).name);    
    data =double(imread(f));
    flat = data(:)';
    X = [X;flat];    
end
k=2:2:24;
n = size(X,1);
d = size(X,2);
bestC = [];
besterror=zeros(12,1);
for j = k
    for i = 1:10    
        [idx,C] = kmeans(X,j,'Start','sample');
        er = geterror(X,idx,C);
       % fprintf('Error for iteration %i with k = %i is %d\n',i,j,er);
        if(besterror(j/2) == 0 )
            besterror(j/2)=er;    
            bestC = C;
        elseif(er < besterror(j/2) && besterror(j/2)~=0)
            besterror(j/2) = er;        
            bestC = C;
        end
    end 
end
plot(k,besterror,'X');
xlabel 'K';
ylabel 'Error';
title 'eror vs. k';


function err = geterror(X,idx,C)   
    err=0;
    d = size(X,1);
    for N = 1:d    
            rnk = idx(N);
            a = norm(X(N,:)-C(rnk,:))^2;
            err = err + a;       
    end
end



