clear;
% load the date
%load('dataLinReg2D.txt');
load('dataQuadReg2D.txt');
% plot it
figure(1);clf;hold on;
%plot3(dataLinReg2D(:,1),dataLinReg2D(:,2),dataLinReg2D(:,3),'r.');
plot3(dataQuadReg2D(:,1),dataQuadReg2D(:,2),dataQuadReg2D(:,3),'r.');
% decompose in input X and output Y
n = size(dataQuadReg2D,1);

X=[];
Y=[];


%size(X)
%size(Y)
X = dataQuadReg2D(:,1:2);
Y = dataQuadReg2D(:,3);
% prepend 1s to inputs

X = [ones(n,1),X,X(:,1).^2,X(:,2).^2,X(:,1).*X(:,2)];
k = size(X,2);
lamda = 1;
B=zeros(k,k);
Lsqrer=zeros(k,1);
lsumsq=0;
squareError= 0
for i = 1:k
    Xi = [X(1:((i-1)*floor(n/k)),:);X(((i)*floor(n/k))+1:n,:)];
    Yi = [Y(1:((i-1)*floor(n/k)),:);Y(((i)*floor(n/k))+1:n,:)];
   % B(i,:) = inv(Xi'*Xi)*Xi'*Yi;
    %Lsqrer(i)= sum(Y(((i-1)*floor(n/k))+1:(i*floor(n/k)),:)-(X(((i-1)*floor(n/k))+1:(i*floor(n/k)),:)*reshape(B(i,:),k,1)).^2);
    %lsumsq = lsumsq+(Lsqrer(i)^2);
    
end

%Msqerk = mean(Lsqrer);
%variancek  = (1/(k-1))*(lsumsq-(k*Msqerk));

%sizei = floor(n/k)
% compute optimal beta
Ik = eye(k);

beta = inv((X'*X)-(lamda*Ik))*X'*Y

for i =1:n
    squareError =squareError+(Y(i)-(X(i,:)*beta))^2;
end 
variance = (1/(n-k)) * squareError;
fprintf('square error is %d\n',squareError);
fprintf('vaiance is %d\n',variance);
%fprintf('square error for k-cross fold is %d\n',Msqerk);
%fprintf('vaiance is for K-cross fold is %d\n',variancek);
% displaynewdata=data(randperm(200)); the function
[a b] = meshgrid(-2:.1:2,-2:.1:2);
Xgrid = [ones(length(a(:)),1),a(:),b(:),a(:).^2,b(:).^2,a(:).*b(:)];
Ygrid = Xgrid*beta;
Ygrid = reshape(Ygrid,size(a));
h = surface(a,b,Ygrid);
view(3);
grid on;