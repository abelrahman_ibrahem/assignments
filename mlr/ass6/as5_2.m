
file = dir('/home/abdo/mlr/ass6/yalefaces');
NF = length(file);
images=[];
%question 2.a
for k = 3 : NF
    f = fullfile('/home/abdo/mlr/ass6/yalefaces',file(k).name);    
    data =double(imread(f));
    flat = data(:)';
    images = [images;flat];    
end

n = size(images,1);
d = size(images,2);
avgFace = zeros(1,size(images,2));
%question 2b
for i = 1:n
    avgFace = avgFace + images(i,:);
end
avgFace = avgFace/n;
In = ones(n,d);
Xtilda = images- avgFace;
%question 2c
[U, S, V] = svd(Xtilda, 'econ');
%q2d
p = 100;
Vp = V(:,1:p);
Z = Xtilda*Vp;
%q2d
Xdash = Z * Vp' +avgFace;

Error = 0;
for i = 1:n
    Error = Error + (norm(images(i,:)-Xdash(i,:)))^2;
    image = vec2mat(Xdash(i,:),243);
    if 7 ~= exist (strcat('/home/abdo/mlr/ass6/yalefaces',num2str(p),'/'), 'dir')
        mkdir(strcat('/home/abdo/mlr/ass6/yalefaces',num2str(p),'/'))
    end
    imagename = fullfile(strcat('/home/abdo/mlr/ass6/yalefaces',num2str(p),'/'),strcat('image',num2str(i),'.gif'));
    imwrite(image,imagename);
end
 fprintf( 'Error is %d for p = %d\n',Error,p)
