data = load('data2Class.txt');
% plot it
figure(1);clf;hold on;
%plot3(dataLinReg2D(:,1),dataLinReg2D(:,2),dataLinReg2D(:,3),'r.');
plot3(data(:,1),data(:,2),data(:,3),'r.');
% decompose in input X and output Y
n = size(data,1);
lamda = 0.1;
X = data(:,1:2);
Y = data(:,3);
% prepend 1s to inputs

X = [ones(n,1),X,X(:,1).^2,X(:,2).^2,X(:,1).*X(:,2)];
%X = [ones(n,1),X];
beta = zeros(size(X,2),1)

for i = 1:5
  hess=Hessian(X,beta,lamda)
  grad = gradient(X,Y,beta,lamda)
  beta = beta+ inv(hess)*grad;
end
beta
[a b] = meshgrid(-2:.1:2,-2:.1:2);
Xgrid = [ones(length(a(:)),1),a(:),b(:),a(:).^2,b(:).^2,a(:).*b(:)];
Ygrid = Xgrid*beta;
Ygrid = reshape(Ygrid,size(a));
h = surface(a,b,Ygrid);
view(3);
grid on;

function sig=sigmoid(x)    
    sig =  1./(1 + exp(-x));
end

function p=P(x,beta)
    v = x * beta;
    p =  sigmoid(v);
end

function w = W(x,beta)
    v= P(x,beta).*(1-P(x,beta));
    w= diag(v);
    
end

function ret= gradient(x,y,beta,lamda)
    ret = (x' *(P(x,beta)-y))+ 2*lamda;
end

function ret= Hessian(x,beta,lamda)
    ret = (x' * W(x,beta) * x)+ 2*lamda;
end

