clc;
X =load('data2Class_adjusted.txt');
N = size(X,1);
M = size(X,2);
h1=100;
W = rand(h1,M)
Y=X(:,4);
X=X(:,1:3);
dl = 0 ;
alpha =0.05;

gradient = [];

for j = 1:500    
    count = 0;
    sumdwl0=zeros(M-1,h1);
    sumdwl1=zeros(h1,1);
    loss=0;
    wl1 = W(:,M)';
    wl0 = W(:,1:M-1);
    for i = 1:N
        
        x=X(i,:)';
        [f,xl1]=forward(x,W);
        
        
        loss = loss + max(0, 1- f * Y(i));
       
       % fprintf('\niteration %d.%d loss = %f f= %d , y= %d',j,i,loss,f,Y(i));
        dL = -Y(i)*(1-Y(i)*f);
        dl2 = backward(dL,xl1,wl1);
        dl1 = backward(dl2,x,wl0);  
        dwl1 = dl2'* f;
        dwl0 = dl1'* xl1';   
        sumdwl0 = sumdwl0+dwl0;
        sumdwl1 = sumdwl1+dwl1;
    end
    fprintf('\nerror for iteration %d is %f\n',j,loss);
    if ~isnan(sumdwl1(1,1)) && ~isnan(sumdwl0(1,1))
            wl0 =wl0 - alpha * sumdwl0';
            wl1 =wl1 - alpha * sumdwl1';    
            W = [wl0,wl1'];
        else 
            break;
     end
    
    if(loss <0.001)
        break;
    end
end
figure
stem3(X(:,2),X(:,3),Y);
surf(X(:,2),X(:,3),Y);

function [f,xl1]= forward(x,w)    
    wl1 = w(:,1:size(x,1));    
    wl2 = w(:,size(x,1)+1)';
    z1 = wl1*x;
    xl1 = sigmf(z1,[1,0]);    
    z2 = wl2*xl1;
    f = sigmf(z2,[1,0]);
end

function res= backward(d,x,w)   
  res = (d*w).*(x.*(1-x))';
end

