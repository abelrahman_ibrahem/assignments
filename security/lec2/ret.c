#include <stdio.h>

#pragma check_stack(off)

void myfunction(){
	char buffer1[8]="aaaaaaaa";
	char buffer2[12]="bbbbbbbbbbbb";
        long *retp:48;

	printf("My stack looks like:\n%p\n%p\n%p\n%p\n%p\n% p\n\n");
	retp = (long*)buffer1 + 2;
	printf("Old retp value: %x at %x\n", *retp,retp);
 	(*retp) += 7;
	 printf("Now the stack looks like:\n%p\n%p\n%p\n%p\n%p\n%p\n\n");
	printf("New retp value: %x at %x\n", *retp,retp);
	
}
int main(){
	int x =0;
	 printf("Address of myfunction = %p\n", myfunction);

	x=0;
	myfunction();
	x=1;


	printf("x is %d",x);
}
