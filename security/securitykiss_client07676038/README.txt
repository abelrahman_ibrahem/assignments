Please find the server list to configure: 
Country  City                 IP Address        Proto   Port   
DE       Darmstadt            46.165.221.230    tcp     443    
DE       Darmstadt            46.165.221.230    udp     5353   
DE       Darmstadt            46.165.221.230    udp     5000   
FR       Paris                176.31.32.106     tcp     443    
FR       Paris                176.31.32.106     udp     5353   
FR       Paris                176.31.32.106     udp     5000   
DE       Frankfurt            46.165.197.1      tcp     443    
DE       Frankfurt            46.165.197.1      udp     5353   
DE       Frankfurt            46.165.197.1      udp     5000   
US       Los Angeles          23.19.26.250      tcp     443    
US       Los Angeles          23.19.26.250      udp     5353   
US       Los Angeles          23.19.26.250      udp     5000   
FR       Reims                195.154.128.163   tcp     443    
FR       Reims                195.154.128.163   udp     5353   
FR       Reims                195.154.128.163   udp     5000   
US       Chicago              69.175.85.2       tcp     443    
US       Chicago              69.175.85.2       udp     5353   
US       Chicago              69.175.85.2       udp     5000   
FR       Roubaix              37.59.58.28       tcp     443    
FR       Roubaix              37.59.58.28       udp     5353   
FR       Roubaix              37.59.58.28       udp     5000   
UK       London               78.129.174.102    tcp     443    
UK       London               78.129.174.102    udp     5353   
UK       London               78.129.174.102    udp     5000   
UK       Maidenhead           217.147.83.43     tcp     443    
UK       Maidenhead           217.147.83.43     udp     5353   
UK       Maidenhead           217.147.83.43     udp     5000   
US       New York             173.234.116.170   tcp     443    
US       New York             173.234.116.170   udp     5353   
US       New York             173.234.116.170   udp     5000   
